/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/15 12:12:11 by maattal           #+#    #+#             */
/*   Updated: 2017/03/09 14:45:42 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H
# define SUCCESS 1
# define ERROR 0
# define SOLVING_GRID_MIN_WIDTH 3
# define TETRA_PATTERNS_NUMBER 8
# define BUFFER_MAX_SIZE 104
# define BUFFER_COLUMN_LENGTH 5
# define BUFFER_ROW_LENGTH 4
# define BUFFER_LENGTH 21
# define TETRA_MAX_NBR 26
# define KNOWN_TETRAS_NBR 19
# define DIRECTIONS_MAX_NBR 3
# define TETRAS_DIRECTIONS_NBR 4
# define TETRA_SIZE 4

# define INDEX_TETRA_SQUARE 0
# define INDEX_TETRA_Z 1
# define INDEX_TETRA_Z_UP 2
# define INDEX_TETRA_Z_DOWN 3
# define INDEX_TETRA_Z_REVERSED 4
# define INDEX_TETRA_T 5
# define INDEX_TETRA_T_UP 6
# define INDEX_TETRA_T_DOWN 7
# define INDEX_TETRA_T_REVERSED 8
# define INDEX_TETRA_L 9
# define INDEX_TETRA_L_HORIZONTAL_LEFT 10
# define INDEX_TETRA_L_VERTICAL_LEFT 11
# define INDEX_TETRA_L_HORIZONTAL_RIGHT 12
# define INDEX_TETRA_L_REVERSED 13
# define INDEX_TETRA_L_REVERSED_HORIZONTAL_LEFT 14
# define INDEX_TETRA_L_REVERSED_VERTICAL_RIGHT 15
# define INDEX_TETRA_L_REVERSED_HORIZONTAL_RIGHT 16
# define INDEX_TETRA_I 17
# define INDEX_TETRA_I_REVERSED 18

# include <stdlib.h>
# include <stdio.h>
# include <fcntl.h>
# include <unistd.h>
# include <string.h>
# include "libft.h"

typedef struct	s_coord
{
	int			x;
	int			y;
}				t_coord;

typedef	struct	s_tetra
{
	t_coord		*directions[TETRAS_DIRECTIONS_NBR];
	char		name;
}				t_tetra;

typedef	struct	s_game_data
{
	t_tetra		*found_tetras[TETRA_MAX_NBR];
	t_tetra		*first_found_tetras[TETRA_MAX_NBR];
	t_tetra		known_tetras[KNOWN_TETRAS_NBR];
	char		**solving_grid;
	int			pieces_nbr;
	int			solving_grid_size;
	int			solving_grid_column_length;
}				t_game_data;

t_game_data		g_game_data;

t_coord			g_north;
t_coord			g_south;
t_coord			g_east;
t_coord			g_west;
t_coord			g_south_west;
t_coord			g_neutral;
t_coord			g_north_east;
t_coord			g_north_west;

t_tetra			g_tetra_square;
t_tetra			g_tetra_z;
t_tetra			g_tetra_z_up;
t_tetra			g_tetra_z_down;
t_tetra			g_tetra_z_reversed;
t_tetra			g_tetra_t;
t_tetra			g_tetra_t_up;
t_tetra			g_tetra_t_down;
t_tetra			g_tetra_t_reversed;
t_tetra			g_tetra_l;
t_tetra			g_tetra_l_horizontal_left;
t_tetra			g_tetra_l_vertical_left;
t_tetra			g_tetra_l_horizontal_right;
t_tetra			g_tetra_l_reversed;
t_tetra			g_tetra_l_reversed_horizontal_left;
t_tetra			g_tetra_l_reversed_vertical_right;
t_tetra			g_tetra_l_reversed_horizontal_right;
t_tetra			g_tetra_i;
t_tetra			g_tetra_i_reversed;

void			init_known_tetraminos_positions();
void			init_cardinals_points();
int				init_game_data();
void			init_found_tetras();
int				open_file(char *file);
int				create_tetraminos(char *buffer);
int				read_file(int *fd);
int				check_all_pattern(char *buffer, int columnm, int row);
int				move_in_pattern(char *buffer,
				int known_pattern_index, int column, int row);
int				fillit(int *argc, char **argv);
int				malloc_game_data();
int				init_known_tetras_tab();
char			get_coords(char *buffer, int x, int y);
void			algorithm();
void			get_next_solving_grid_coord(t_coord current_coord);
t_coord			*create_solving_coord(int x, int y);
int				solving(t_coord algo_coord, t_coord *solving_coord,
				int found_tetras_index);
void			put_tetra(t_coord *current_grid_coord, int found_tetras_index);
int				check_buffer(char *buffer);
t_coord			*find_next_free_space(t_coord *solving_coord);
t_coord			*init_new_coord(int x, int y);
void			display_2d_array(char **tab);
char			**init_2d_array(int len);
void			clean_2d_array(char tetra_name);
void			init_known_tetras(void);
void			init_known_tetraminos_positions(void);
void			init_known_tetraminos_positions_2(void);
void			init_known_tetraminos_positions_3(void);
void			init_known_tetraminos_positions_4(void);
void			init_known_tetraminos_positions_5(void);
t_coord			coord_incrementation(t_coord new_coord,
				int t_index, int d_index);
int				check_if_coord_in_array(t_coord new_coord);

#endif
