NO_COLOR=\033[0m
OK_COLOR=\033[32;01m
ERROR_COLOR=\033[31;01m
IN_PROGRESS_COLOR=\033[33;01m

FILLIT = fillit

NAME = $(FILLIT)

LIBFT = libft.a

CFLAGS = -Wall -Werror -Wextra

CC = gcc

RM = rm -rf

SRCS = main.c \
	   algorithm.c \
	   fillit.c \
	   array_functions.c \
	   coords_functions.c \
	   init_tetraminos.c \
	   patterns.c\
	   read_file.c \
	   tetraminos.c 

SRCDIR = ./src/
OBJDIR = ./src/

OBJS = $(addprefix $(OBJDIR)/, $(SRCS:.c=.o))

all: $(NAME)
$(NAME): $(LIBFT) $(OBJS)
	@echo "Compilation of Fillit :$(IN_PROGRESS_COLOR) [IN PROGRESS] $(NO_COLOR)"
	@$(CC) $(CFLAGS) -Llibft $(OBJS) -lft -o $(NAME)
	@echo "$(SRCS)"
ifeq ($(NAME), fillit)
	@echo "Compilation of Fillit :$(OK_COLOR) [SUCCESS] $(NO_COLOR)"
else
	@echo "Compilation of Fillit :$(ERROR_COLOR) [ERROR] $(NO_COLOR)"
endif

$(addprefix $(OBJDIR)/, %.o): $(addprefix $(SRCDIR)/, %.c)
	@mkdir -p $(OBJDIR)
	@$(CC) $(CFLAGS) -o $@ -c $^

$(LIBFT):
	@echo "Compilation of Libft :$(IN_PROGRESS_COLOR) [IN PROGRESS] $(NO_COLOR)"
	@make -C ./libft/
ifeq ($(LIBFT), libft.a)
	@echo "Compilation of Libft :$(OK_COLOR) [SUCCESS] $(NO_COLOR)"
else
	@echo "Compilation of Libft :$(ERROR_COLOR) [ERROR] $(NO_COLOR)"
endif

clean:
	@$(RM) $(OBJS)
	@make clean -C ./libft/

fclean: clean
	@$(RM) $(LIBFT)
	@$(RM) $(NAME)
	@make fclean -C ./libft/
	@echo "Fillit :$(OK_COLOR) [CLEANED]$(NO_COLOR)"

re: fclean all

.PHONY: fclean all re clean
