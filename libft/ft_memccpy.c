/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 11:15:19 by maattal           #+#    #+#             */
/*   Updated: 2017/03/30 13:53:35 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	char	*ptr0;
	char	*ptr1;
	size_t	buffer;

	ptr0 = (char *)dst;
	ptr1 = (char *)src;
	buffer = 0;
	while (buffer < n)
	{
		if (ptr1[buffer] == (char)c)
		{
			ptr0[buffer] = ptr1[buffer];
			return (&(ptr0[buffer + 1]));
		}
		ptr0[buffer] = ptr1[buffer];
		buffer++;
	}
	return (NULL);
}
