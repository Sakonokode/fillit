/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/16 11:10:41 by maattal           #+#    #+#             */
/*   Updated: 2017/03/30 13:51:30 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static char		*ft_make_str(int n, int *i, int *operator, char *str)
{
	int	length;

	length = n;
	if (n < 0)
	{
		operator[0] = -1;
		*i = 2;
	}
	while ((length = length / 10) != 0)
		(*i)++;
	str = (char *)malloc(sizeof(char) * (*i + 1));
	return (str);
}

char			*ft_itoa(int n)
{
	char			*str;
	int				i;
	int				operator[1];
	unsigned int	j;

	str = NULL;
	operator[0] = 1;
	i = 1;
	j = n;
	str = ft_make_str(n, &i, operator, str);
	if (str)
	{
		if (operator[0] == -1)
			j = n * -1;
		str[i] = '\0';
		while (i-- >= 0)
		{
			str[i] = j % 10 + '0';
			j = j / 10;
		}
		if (operator[0] == -1)
			str[0] = '-';
	}
	return (str);
}
