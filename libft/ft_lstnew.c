/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 14:46:20 by maattal           #+#    #+#             */
/*   Updated: 2017/03/30 13:53:00 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*ptr;

	ptr = (t_list*)malloc(sizeof(t_list));
	if (ptr == NULL)
		return (NULL);
	if (content == NULL)
	{
		ptr->content_size = 0;
		ptr->content = NULL;
	}
	else
	{
		ptr->content_size = content_size;
		ptr->content = malloc(content_size);
		ft_memcpy(ptr->content, content, content_size);
	}
	ptr->next = NULL;
	return (ptr);
}
