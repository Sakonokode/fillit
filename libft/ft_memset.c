/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 11:08:01 by maattal           #+#    #+#             */
/*   Updated: 2017/03/30 13:53:56 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	size_t	buffer;
	char	*ptr;

	buffer = 0;
	ptr = (char *)b;
	while (buffer < len)
	{
		ptr[buffer] = (char)c;
		buffer++;
	}
	return (b);
}
