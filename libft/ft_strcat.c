/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:52:41 by maattal           #+#    #+#             */
/*   Updated: 2017/03/30 13:55:22 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strcat(char *s1, const	char *s2)
{
	int size;
	int buffer;

	size = 0;
	buffer = 0;
	size = ft_strlen(s1);
	while (s2[buffer])
	{
		s1[size + buffer] = s2[buffer];
		buffer++;
	}
	s1[size + buffer] = '\0';
	return ((char *)s1);
}
