/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 11:25:14 by maattal           #+#    #+#             */
/*   Updated: 2017/03/30 13:53:50 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	int		i;

	if ((long int)dst > (long int)src)
	{
		i = (int)len;
		while (--i > -1)
			((char *)dst)[i] = ((char *)src)[i];
	}
	else
	{
		i = -1;
		while (++i < (int)len)
			((char *)dst)[i] = ((char *)src)[i];
	}
	return (dst);
}
