/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 16:35:43 by maattal           #+#    #+#             */
/*   Updated: 2017/03/30 13:57:40 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int		ft_strnequ(char const *s1, char const *s2, size_t n)
{
	size_t	i;

	i = 0;
	if (s1 == NULL && s2 == NULL)
		return (1);
	if (s1 && s2)
	{
		while (i < n)
		{
			if (s1[i] == s2[i])
				i++;
			else
				return (0);
		}
	}
	if (i == n)
		return (1);
	return (0);
}
