/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:34:40 by maattal           #+#    #+#             */
/*   Updated: 2017/03/30 13:57:32 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strncpy(char *dst, const char *src, size_t len)
{
	unsigned int		buffer;

	buffer = 0;
	while (buffer < len && src[buffer])
	{
		dst[buffer] = src[buffer];
		buffer++;
	}
	if (buffer < len)
	{
		while (buffer < len)
		{
			dst[buffer] = '\0';
			buffer++;
		}
	}
	return (dst);
}
