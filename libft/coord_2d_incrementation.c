/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   coord_2d_incrementation.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/09 11:01:00 by maattal           #+#    #+#             */
/*   Updated: 2017/03/30 13:50:07 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int	coord_2d_incrementation(int *coord, int tab_size)
{
	if (coord->x >= 0 && coord->x < tab_size
			&& coord->y >= 0 && coord->y < tab_size)
	{
		coord->x++;
		if ((coord->x == tab_size)
				&& (coord->y != tab_size - 1))
		{
			coord->x = 0;
			coord->y++;
		}
		return (SUCCESS);
	}
	return (ERROR);
}
