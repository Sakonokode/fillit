/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 11:37:43 by maattal           #+#    #+#             */
/*   Updated: 2017/03/30 13:57:58 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	char	*cpy;
	char	*buf;
	size_t	length;

	buf = (char *)(big + len);
	length = ft_strlen(little);
	cpy = (char *)big;
	if (little[0] == '\0')
		return ((char *)big);
	while ((cpy = ft_strchr(cpy, *little)) && cpy <= buf)
	{
		if ((cpy + length <= buf) && !ft_strncmp(cpy, little, length))
			return (cpy);
		cpy++;
	}
	return (NULL);
}
