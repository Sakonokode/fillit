/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 14:19:51 by maattal           #+#    #+#             */
/*   Updated: 2017/03/30 13:58:05 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strrchr(char const *s, int c)
{
	char *tmp;

	tmp = (char *)s + ft_strlen(s) + 1;
	while (--tmp > s - 1)
		if (*tmp == (char)c)
			return (tmp);
	return (NULL);
}
