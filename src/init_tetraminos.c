/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_tetraminos.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/01 15:12:53 by maattal           #+#    #+#             */
/*   Updated: 2017/03/01 15:25:19 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

void	init_known_tetraminos_positions(void)
{
	init_known_tetraminos_positions_2();
	init_known_tetraminos_positions_3();
	init_known_tetraminos_positions_4();
	init_known_tetraminos_positions_5();
}

void	init_known_tetraminos_positions_2(void)
{
	g_tetra_t.directions[0] = &g_east;
	g_tetra_t.directions[1] = &g_east;
	g_tetra_t.directions[2] = &g_south_west;
	g_tetra_t.directions[3] = NULL;
	g_tetra_t_up.directions[0] = &g_south;
	g_tetra_t_up.directions[1] = &g_south;
	g_tetra_t_up.directions[2] = &g_north_east;
	g_tetra_t_up.directions[3] = NULL;
	g_tetra_t_down.directions[0] = &g_south;
	g_tetra_t_down.directions[1] = &g_south;
	g_tetra_t_down.directions[2] = &g_north_west;
	g_tetra_t_down.directions[3] = NULL;
	g_tetra_t_reversed.directions[0] = &g_south_west;
	g_tetra_t_reversed.directions[1] = &g_east;
	g_tetra_t_reversed.directions[2] = &g_east;
	g_tetra_t_reversed.directions[3] = NULL;
	g_tetra_l.directions[0] = &g_south;
	g_tetra_l.directions[1] = &g_south;
	g_tetra_l.directions[2] = &g_east;
	g_tetra_l.directions[3] = NULL;
}

void	init_known_tetraminos_positions_3(void)
{
	g_tetra_l_horizontal_left.directions[0] = &g_south;
	g_tetra_l_horizontal_left.directions[1] = &g_north_east;
	g_tetra_l_horizontal_left.directions[2] = &g_east;
	g_tetra_l_horizontal_left.directions[3] = NULL;
	g_tetra_l_vertical_left.directions[0] = &g_east;
	g_tetra_l_vertical_left.directions[1] = &g_south;
	g_tetra_l_vertical_left.directions[2] = &g_south;
	g_tetra_l_vertical_left.directions[3] = NULL;
	g_tetra_l_horizontal_right.directions[0] = &g_south;
	g_tetra_l_horizontal_right.directions[1] = &g_west;
	g_tetra_l_horizontal_right.directions[2] = &g_west;
	g_tetra_l_horizontal_right.directions[3] = NULL;
	g_tetra_l_reversed.directions[0] = &g_south;
	g_tetra_l_reversed.directions[1] = &g_south;
	g_tetra_l_reversed.directions[2] = &g_west;
	g_tetra_l_reversed.directions[3] = NULL;
	g_tetra_l_reversed_horizontal_left.directions[0] = &g_south;
	g_tetra_l_reversed_horizontal_left.directions[1] = &g_east;
	g_tetra_l_reversed_horizontal_left.directions[2] = &g_east;
	g_tetra_l_reversed_horizontal_left.directions[3] = NULL;
}

void	init_known_tetraminos_positions_4(void)
{
	g_tetra_l_reversed_vertical_right.directions[0] = &g_east;
	g_tetra_l_reversed_vertical_right.directions[1] = &g_south_west;
	g_tetra_l_reversed_vertical_right.directions[2] = &g_south;
	g_tetra_l_reversed_vertical_right.directions[3] = NULL;
	g_tetra_l_reversed_horizontal_right.directions[0] = &g_east;
	g_tetra_l_reversed_horizontal_right.directions[1] = &g_east;
	g_tetra_l_reversed_horizontal_right.directions[2] = &g_south;
	g_tetra_l_reversed_horizontal_right.directions[3] = NULL;
	g_tetra_i.directions[0] = &g_south;
	g_tetra_i.directions[1] = &g_south;
	g_tetra_i.directions[2] = &g_south;
	g_tetra_i.directions[3] = NULL;
	g_tetra_i_reversed.directions[0] = &g_east;
	g_tetra_i_reversed.directions[1] = &g_east;
	g_tetra_i_reversed.directions[2] = &g_east;
	g_tetra_i_reversed.directions[3] = NULL;
}

void	init_known_tetraminos_positions_5(void)
{
	g_tetra_square.directions[0] = &g_east;
	g_tetra_square.directions[1] = &g_south;
	g_tetra_square.directions[2] = &g_west;
	g_tetra_square.directions[3] = NULL;
	g_tetra_z.directions[0] = &g_east;
	g_tetra_z.directions[1] = &g_south;
	g_tetra_z.directions[2] = &g_east;
	g_tetra_z.directions[3] = NULL;
	g_tetra_z_up.directions[0] = &g_south;
	g_tetra_z_up.directions[1] = &g_west;
	g_tetra_z_up.directions[2] = &g_south;
	g_tetra_z_up.directions[3] = NULL;
	g_tetra_z_down.directions[0] = &g_south;
	g_tetra_z_down.directions[1] = &g_east;
	g_tetra_z_down.directions[2] = &g_south;
	g_tetra_z_down.directions[3] = NULL;
	g_tetra_z_reversed.directions[0] = &g_east;
	g_tetra_z_reversed.directions[1] = &g_south_west;
	g_tetra_z_reversed.directions[2] = &g_west;
	g_tetra_z_reversed.directions[3] = NULL;
}
