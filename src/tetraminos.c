/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tetraminos.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/19 11:25:35 by maattal           #+#    #+#             */
/*   Updated: 2017/03/30 13:39:33 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

void	init_cardinals_points(void)
{
	g_neutral.x = 0;
	g_neutral.y = 0;
	g_north.x = 0;
	g_north.y = -1;
	g_south.x = 0;
	g_south.y = 1;
	g_east.x = 1;
	g_east.y = 0;
	g_west.x = -1;
	g_west.y = 0;
	g_south_west.x = g_south.x + g_west.x;
	g_south_west.y = g_south.y + g_west.y;
	g_north_east.x = g_north.x + g_east.x;
	g_north_east.y = g_north.y + g_east.y;
	g_north_west.x = g_north.x + g_west.x;
	g_north_west.y = g_north.y + g_west.y;
}

void	init_known_tetras(void)
{
	g_game_data.pieces_nbr = 0;
	g_game_data.known_tetras[0] = g_tetra_square;
	g_game_data.known_tetras[1] = g_tetra_z;
	g_game_data.known_tetras[2] = g_tetra_z_up;
	g_game_data.known_tetras[3] = g_tetra_z_down;
	g_game_data.known_tetras[4] = g_tetra_z_reversed;
	g_game_data.known_tetras[5] = g_tetra_t;
	g_game_data.known_tetras[6] = g_tetra_t_up;
	g_game_data.known_tetras[7] = g_tetra_t_down;
	g_game_data.known_tetras[8] = g_tetra_t_reversed;
	g_game_data.known_tetras[9] = g_tetra_l;
	g_game_data.known_tetras[10] = g_tetra_l_horizontal_left;
	g_game_data.known_tetras[11] = g_tetra_l_vertical_left;
	g_game_data.known_tetras[12] = g_tetra_l_horizontal_right;
	g_game_data.known_tetras[13] = g_tetra_l_reversed;
	g_game_data.known_tetras[14] = g_tetra_l_reversed_horizontal_left;
	g_game_data.known_tetras[15] = g_tetra_l_reversed_vertical_right;
	g_game_data.known_tetras[16] = g_tetra_l_reversed_horizontal_right;
	g_game_data.known_tetras[17] = g_tetra_i;
	g_game_data.known_tetras[18] = g_tetra_i_reversed;
}

int		init_game_data(int *argc, char **argv)
{
	init_cardinals_points();
	init_known_tetraminos_positions();
	init_known_tetras();
	init_found_tetras();
	if (*argc >= 2)
	{
		if (open_file(argv[1]) == SUCCESS)
		{
			g_game_data.solving_grid_size = g_game_data.pieces_nbr * TETRA_SIZE;
			g_game_data.solving_grid_column_length = 2;
		}
		else
		{
			ft_putstr("error\n");
			return (ERROR);
		}
	}
	else
	{
		ft_putstr("usage: ./fillit tetriminos_file\n");
		return (ERROR);
	}
	g_game_data.solving_grid =
		init_2d_array(g_game_data.solving_grid_column_length);
	return (SUCCESS);
}

void	init_found_tetras(void)
{
	int index;

	index = 0;
	while (index < TETRA_MAX_NBR)
	{
		g_game_data.found_tetras[index] = NULL;
		index++;
	}
}
