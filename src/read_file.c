/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/28 11:32:47 by maattal           #+#    #+#             */
/*   Updated: 2017/03/01 13:46:34 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

int		open_file(char *file)
{
	int	fd;

	fd = open(file, O_RDONLY);
	if (fd < 0)
		return (ERROR);
	if (read_file(&fd) == SUCCESS)
	{
		close(fd);
		return (SUCCESS);
	}
	close(fd);
	return (ERROR);
}

int		read_file(int *fd)
{
	char	*buffer;
	int		last_read;
	int		actual_read;
	int		i;

	i = 0;
	last_read = -1;
	if ((buffer = (char*)malloc(sizeof(char) * (BUFFER_LENGTH))) == NULL)
		return (ERROR);
	while ((actual_read = read(*fd, buffer, BUFFER_LENGTH)))
	{
		last_read = actual_read;
		if (create_tetraminos(buffer) == ERROR)
			return (ERROR);
		i++;
	}
	if (last_read != (BUFFER_LENGTH - 1) || actual_read != 0)
		return (ERROR);
	return (SUCCESS);
}

char	get_coords(char *buffer, int column, int row)
{
	return (buffer[row * BUFFER_COLUMN_LENGTH + column]);
}

int		create_tetraminos(char *buffer)
{
	int		column;
	int		row;

	row = 0;
	while (row < BUFFER_ROW_LENGTH)
	{
		column = 0;
		while (column < BUFFER_COLUMN_LENGTH)
		{
			if (check_buffer(buffer) == ERROR)
				return (ERROR);
			if (get_coords(buffer, column, row) == '#')
			{
				if (check_all_pattern(buffer, column, row) == SUCCESS)
					return (SUCCESS);
				else
					return (ERROR);
			}
			column++;
		}
		row++;
	}
	return (ERROR);
}

int		check_buffer(char *buffer)
{
	int i;
	int sharp_counter;
	int dot_counter;

	i = 0;
	dot_counter = 0;
	sharp_counter = 0;
	while (buffer[i])
	{
		if (buffer[i] == '#')
			sharp_counter++;
		if (buffer[i] == '.')
			dot_counter++;
		i++;
	}
	if (dot_counter != 12)
		return (ERROR);
	if (sharp_counter != 4)
		return (ERROR);
	return (SUCCESS);
}
