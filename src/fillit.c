/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 14:53:21 by maattal           #+#    #+#             */
/*   Updated: 2017/03/09 13:31:04 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

int		fillit(int *argc, char **argv)
{
	t_coord		algo_coord;

	algo_coord.x = 0;
	algo_coord.y = 0;
	if (init_game_data(argc, argv) == ERROR)
		return (ERROR);
	algorithm(algo_coord);
	display_2d_array(g_game_data.solving_grid);
	return (SUCCESS);
}
