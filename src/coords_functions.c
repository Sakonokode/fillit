/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   coords_functions.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/01 15:31:08 by maattal           #+#    #+#             */
/*   Updated: 2017/03/09 14:42:49 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

t_coord		*find_next_free_space(t_coord *solving_coord)
{
	t_coord *ret;
	int		i;
	int		j;

	ret = init_new_coord(-1, -1);
	i = solving_coord->y;
	j = solving_coord->x + 1;
	while (g_game_data.solving_grid[i])
	{
		while (g_game_data.solving_grid[i][j])
		{
			if (g_game_data.solving_grid[i][j] == '.')
			{
				ret->x = j;
				ret->y = i;
				return (ret);
			}
			j++;
		}
		j = 0;
		i++;
	}
	return (ret);
}

t_coord		*init_new_coord(int x, int y)
{
	t_coord *ret;

	ret = (t_coord*)malloc(sizeof(t_coord));
	ret->x = x;
	ret->y = y;
	return (ret);
}

int			check_if_coord_in_array(t_coord new_coord)
{
	if (new_coord.x >= 0 && new_coord.x <
			(g_game_data.solving_grid_column_length) && new_coord.y >= 0
			&& new_coord.y < g_game_data.solving_grid_column_length)
		return (1);
	return (0);
}

t_coord		coord_incrementation(t_coord new_coord, int t_index, int d_index)
{
	new_coord.x += g_game_data.found_tetras
		[t_index]->directions[d_index]->x;
	new_coord.y += g_game_data.found_tetras
		[t_index]->directions[d_index]->y;
	return (new_coord);
}
