/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   2d_array_functions.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/01 15:20:12 by maattal           #+#    #+#             */
/*   Updated: 2017/03/27 12:50:38 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

void	clean_2d_array(char tetra_name)
{
	int i;
	int j;

	i = 0;
	while (g_game_data.solving_grid[i])
	{
		j = 0;
		while (g_game_data.solving_grid[i][j])
		{
			if (g_game_data.solving_grid[i][j] == tetra_name)
				g_game_data.solving_grid[i][j] = '.';
			j++;
		}
		i++;
	}
}

char	**init_2d_array(int len)
{
	int		i;
	int		j;
	char	**result;

	i = 0;
	result = (char**)malloc(sizeof(char*) * len + 1);
	if (!result)
		return (NULL);
	result[len] = NULL;
	while (i < len)
	{
		result[i] = (char*)malloc(sizeof(char) * len);
		if (!result[i])
			return (NULL);
		j = 0;
		while (j < len)
		{
			result[i][j] = '.';
			j++;
		}
		result[i][j] = '\0';
		i++;
	}
	return (result);
}

void	display_2d_array(char **array)
{
	int i;
	int j;

	i = 0;
	while (array[i])
	{
		j = 0;
		while (array[i][j])
		{
			ft_putchar(array[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}
