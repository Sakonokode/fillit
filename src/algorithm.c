/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algorithm.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: axalves <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 15:26:58 by axalves           #+#    #+#             */
/*   Updated: 2017/03/09 14:45:30 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"
#include <stdio.h>

int			tetra_is_putable(t_coord *current_grid_coord, int t_index)
{
	int		d_index;
	t_coord	new_coord;

	d_index = 0;
	new_coord.x = current_grid_coord->x;
	new_coord.y = current_grid_coord->y;
	if (g_game_data.solving_grid[new_coord.y][new_coord.x] != '.')
		return (ERROR);
	while (d_index < TETRAS_DIRECTIONS_NBR)
	{
		if (g_game_data.found_tetras[t_index]->directions[d_index])
		{
			new_coord = coord_incrementation(new_coord, t_index, d_index);
			if (check_if_coord_in_array(new_coord) == 1)
			{
				if (g_game_data.solving_grid[new_coord.y][new_coord.x] != '.')
					return (ERROR);
			}
			else
				return (ERROR);
		}
		d_index++;
	}
	return (SUCCESS);
}

void		put_tetra(t_coord *current_grid_coord, int found_tetras_index)
{
	int		direction_index;
	t_coord	current_grid_position;
	char	tetra_name;
	t_tetra	*tetra_to_put;

	tetra_to_put = g_game_data.found_tetras[found_tetras_index];
	tetra_name = tetra_to_put->name;
	direction_index = 0;
	current_grid_position.x = current_grid_coord->x;
	current_grid_position.y = current_grid_coord->y;
	g_game_data.solving_grid[current_grid_position.y]
		[current_grid_position.x] = tetra_name;
	while (direction_index < DIRECTIONS_MAX_NBR)
	{
		current_grid_position.x += tetra_to_put->directions[direction_index]->x;
		current_grid_position.y += tetra_to_put->directions[direction_index]->y;
		g_game_data.solving_grid[current_grid_position.y]
			[current_grid_position.x] = tetra_name;
		direction_index++;
	}
}

t_coord		*create_solving_coord(int x, int y)
{
	t_coord	*to_return;

	to_return = (t_coord*)malloc(sizeof(t_coord));
	to_return->x = x;
	to_return->y = y;
	return (to_return);
}

int			solving(t_coord algo_coord, t_coord *solving_coord,
		int found_tetras_index)
{
	if (found_tetras_index == g_game_data.pieces_nbr)
		return (SUCCESS);
	if (solving_coord->x < 0)
		return (ERROR);
	if (tetra_is_putable(solving_coord, found_tetras_index))
	{
		put_tetra(solving_coord, found_tetras_index);
		if (solving(algo_coord, &algo_coord, found_tetras_index + 1))
			return (SUCCESS);
		clean_2d_array(g_game_data.found_tetras[found_tetras_index]->name);
	}
	return (solving(algo_coord, find_next_free_space(solving_coord),
				found_tetras_index));
}

void		algorithm(t_coord algo_coord)
{
	t_coord *solving_coord;
	int		found_tetras_index;

	found_tetras_index = 0;
	solving_coord = init_new_coord(0, 0);
	while (!(solving(algo_coord, solving_coord, found_tetras_index)))
	{
		g_game_data.solving_grid_column_length++;
		g_game_data.solving_grid =
			init_2d_array(g_game_data.solving_grid_column_length);
	}
}
