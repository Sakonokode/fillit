/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   patterns.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/19 11:25:17 by maattal           #+#    #+#             */
/*   Updated: 2017/03/01 13:45:32 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

t_coord			*get_coord_from_direction(t_coord *position, t_coord *direction)
{
	t_coord		*new_coord;

	new_coord = malloc(sizeof(t_coord*));
	if (new_coord == NULL)
		return (NULL);
	new_coord->x = position->x + direction->x;
	new_coord->y = position->y + direction->y;
	return (new_coord);
}

void			copy_directions(int index)
{
	int	d_index;

	d_index = 0;
	while (g_game_data.known_tetras[index].directions[d_index])
	{
		g_game_data.found_tetras[g_game_data.pieces_nbr]->directions[d_index] =
			g_game_data.known_tetras[index].directions[d_index];
		d_index++;
	}
}

int				check_all_pattern(char *buffer, int column, int row)
{
	int			index;

	index = 0;
	while (index < KNOWN_TETRAS_NBR)
	{
		if (move_in_pattern(buffer, index, column, row) == SUCCESS)
		{
			g_game_data.found_tetras[g_game_data.pieces_nbr] =
				malloc(sizeof(t_tetra));
			copy_directions(index);
			g_game_data.found_tetras[g_game_data.pieces_nbr]->name =
				'A' + g_game_data.pieces_nbr;
			g_game_data.pieces_nbr++;
			return (SUCCESS);
		}
		index++;
	}
	return (ERROR);
}

int				move_in_pattern(char *buffer, int known_pattern_index,
		int column, int row)
{
	t_coord		current_position;
	int			index;
	int			sharp_counter;

	sharp_counter = 0;
	index = 0;
	current_position.x = column;
	current_position.y = row;
	while (g_game_data.known_tetras[known_pattern_index].directions[index])
	{
		current_position.x = current_position.x + g_game_data.known_tetras
			[known_pattern_index].directions[index]->x;
		current_position.y = current_position.y + g_game_data.known_tetras
			[known_pattern_index].directions[index]->y;
		if (get_coords(buffer, current_position.x, current_position.y) == '#')
			sharp_counter++;
		index++;
	}
	if (sharp_counter == TETRA_SIZE - 1)
		return (SUCCESS);
	return (ERROR);
}
